from config import keys
from config import products
from selenium import webdriver
import time
import timeit
from tkinter import *
import tkinter.simpledialog


def order(k):
    time.sleep(.5)
    driver.find_element_by_xpath('//*[@id="cart"]/a[2]').click()
    driver.find_element_by_xpath('//*[@id="order_billing_name"]').send_keys(k['name'])
    driver.find_element_by_xpath('//*[@id="order_email"]').send_keys(k['email'])
    driver.find_element_by_xpath('//*[@id="order_tel"]').send_keys(k['phone'])
    driver.find_element_by_xpath('//*[@id="bo"]').send_keys(k['address'])
    driver.find_element_by_xpath('//*[@id="order_billing_zip"]').send_keys(k['zip'])
    driver.find_element_by_xpath('//*[@id="order_billing_city"]').send_keys(k['city'])
    driver.find_element_by_xpath('//*[@id="order_billing_state"]/option[6]').click()
    driver.find_element_by_xpath('//*[@id="cart-cc"]/fieldset/p[2]/label/div/ins').click()

def add():
    driver.find_element_by_xpath('//*[@id="add-remove-buttons"]/input').click()


def start(name, color, style):    
    num = len(products)
    driver.get("https://www.supremenewyork.com/shop/all/" + style)
    time.sleep(.5)
    elem = driver.find_element_by_partial_link_text(name)  
    elem.click()
    time.sleep(.5)
    elem1 = driver.find_element_by_xpath('//a[@data-style-name="'+color+'"]')
    elem1.click()
    time.sleep(.5)
    add()
    order(keys)
    stop = time.time()
    print("Total execution time: {}".format(stop - startertime))



def startlink(link):
    driver.get(link)
    add()
    order(keys)





def save_style():
    startertime = time.time()
    clicked = Lb.curselection()
    print(Lb.get(clicked))
    style = Lb.get(clicked)
    one = str(name.get())
    two = str(color.get())
    start(one,two,style)
    quit()

def save_style1():
    url = str(url_link.get())
    startlink(url)
    quit()
    

def quitall():
    quit()
    driver.quit()

    

if __name__ == '__main__':
    startertime = 0
    driver = webdriver.Chrome('./chromedriver')
    root = Tk()
    root.title("EM Supreme Bot")
    root.geometry("640x640+0+0")
    heading = Label(root, text="Supreme Bot v1", font=("arial", 40, "bold")).pack()
    label1 = Label(root, text="Style of Product", font=("arial", 15,"bold")).pack()
    Lb = Listbox(root) 
    Lb.insert(1, 'Jackets')
    Lb.insert(2, 'Shirts')  
    Lb.insert(2, 'Tops_Sweaters')
    Lb.insert(3, 'Sweatshirts') 
    Lb.insert(2, 'Pants')
    Lb.insert(2, 'T-Shirts') 
    Lb.insert(2, 'Hats') 
    Lb.insert(2, 'Bags') 
    Lb.insert(4, 'Accessories') 
    Lb.insert(2, 'Shoes') 
    Lb.insert(2, 'Skate') 
    Lb.pack() 
    label2 = Label(root, text="Name of Product", font=("arial", 15,"bold")).pack()
    name = StringVar()
    entry_box1 = Entry(root, textvariable=name).pack()
    label3 = Label(root, text="Color of Product", font=("arial", 15,"bold")).pack()
    color = StringVar()
    entry_box2 = Entry(root, textvariable=color).pack()
    button = Button(root, text="Select", command=save_style).pack()
    label4 = Label(root, text="OR", font=("arial", 20,"bold")).place(x = 300, y = 400)
    label3 = Label(root, text="Product URL", font=("arial", 15,"bold")).place(x = 270, y = 450)
    url_link = StringVar()
    entry_box3 = Entry(root, textvariable=url_link).place(x=223,y=480)
    label5 = Label(root, text="MADE BY EDDIE MUN | eddiejmun@gmail.com", font=("arial", 10,"bold")).place(x=200,y=600)
    button1 = Button(root, text="Select", command=save_style1).place(x = 295, y=509)
    button2 = Button(root, text="Quit", command=quitall).place(x = 300, y=575)




    root.mainloop()


